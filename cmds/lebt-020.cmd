require npm, 1.1.0
require adaravis

epicsEnvSet("SECSUB",   "LEBT-020")
epicsEnvSet("HCAMIP",   "FLIR-Blackfly S BFS-PGE-31S4M-20356218")
epicsEnvSet("VCAMIP",   "")
epicsEnvSet("HCAMDRV",  "Aravis")
epicsEnvSet("VCAMDRV",  "")
epicsEnvSet("HCAM",     "HCAM")  # Horizontal plane camera name
epicsEnvSet("VCAM",     "VCAM")  # Vertical plane camera name
epicsEnvSet("HLENSPOS", "PBI-FPM01:Ctrl-ECAT-100:Axis3")
epicsEnvSet("VLENSPOS", "PBI-FPM01:Ctrl-ECAT-100:Axis4")

####### Load Image acquisition

# Pass "DISABLE_VER=#" or "DISABLE_HOR=#" if you want to comment out loading one plane.
iocshLoad("$(npm_DIR)/npm.iocsh", "SECSUB=$(SECSUB), HCAMIP=$(HCAMIP), VCAMIP=$(VCAMIP), HCAM=$(HCAM), VCAM=$(VCAM), DISABLE_VER=#")
