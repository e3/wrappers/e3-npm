require npm, 1.1.0
require adprosilica

epicsEnvSet("SECSUB",   "LEBT-010")
epicsEnvSet("HCAMIP",   "192.5.95.95")
epicsEnvSet("VCAMIP",   "192.5.96.96")
epicsEnvSet("HCAMDRV",  "Prosilica")
epicsEnvSet("VCAMDRV",  "Prosilica")
epicsEnvSet("HCAM",     "HCAM")         // Horizontal plane camera name
epicsEnvSet("VCAM",     "VCAM")         // Vertical plane camera name
epicsEnvSet("HLENSPOS", "PBI-FPM01:Ctrl-ECAT-100:Axis1")
epicsEnvSet("VLENSPOS", "PBI-FPM01:Ctrl-ECAT-100:Axis2")

####### Load Image acquisition

# Pass "DISABLE_VER=#" or "DISABLE_HOR=#" if you want to comment out loading one plane.
iocshLoad("$(npm_DIR)/npm.iocsh", "SECSUB=$(SECSUB), HCAMIP=$(HCAMIP), VCAMIP=$(VCAMIP), HCAM=$(HCAM), VCAM=$(VCAM)")
