require npm, 1.1.0
require adsimdetector

epicsEnvSet("SECSUB",   "NPM-SIM")
epicsEnvSet("HCAMIP",   "")
epicsEnvSet("VCAMIP",   "")
epicsEnvSet("HCAMDRV",  "SimDetector")
epicsEnvSet("VCAMDRV",  "SimDetector")
epicsEnvSet("HCAM",     "HCAM")         // Horizontal plane camera name
epicsEnvSet("VCAM",     "VCAM")         // Vertical plane camera name

####### Load Image acquisition

# Pass "DISABLE_VER=#" or "DISABLE_HOR=#" if you want to comment out loading one plane.
iocshLoad("$(npm_DIR)/npm.iocsh", "SECSUB=$(SECSUB), HCAMIP=$(HCAMIP), VCAMIP=$(VCAMIP), HCAM=$(HCAM), VCAM=$(VCAM)")
