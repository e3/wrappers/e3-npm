#ifndef NDPluginTrajectory_H
#define NDPluginTrajectory_H

#include <epicsTypes.h>
#include "NDPluginDriver.h"

#define NDPluginTrajectoryProfileAxisString          "PROFILE_AXIS"           /* (asynUInt32,       r/w) Profile axis */
#define NDPluginTrajectoryNumProfilesString          "NUM_PROFILES"           /* (asynUInt32,       r/w) Number of profile slices */
#define NDPluginTrajectoryPositionString             "POSITION"               /* (asynFloat64,      r/o) Beam position (px) */
#define NDPluginTrajectoryAngleString                "ANGLE"                  /* (asynFloat64,      r/o) Beam angle (mrad) */
#define NDPluginTrajectoryEmittanceString            "EMITTANCE"              /* (asynFloat64,      r/o) Emittance (px * mrad) */
#define NDPluginTrajectoryProfileIndexString         "INDEX"                  /* (asynFloat64Array, r/o) Profile index for display in wavefoms*/
#define NDPluginTrajectoryProfileWaveformString      "PROFILE_WF"             /* (asynFloat64Array, r/o) Profile[i] waveform */
#define NDPluginTrajectoryCurveWaveformString        "CURVE_WF"               /* (asynFloat64Array, r/o) Fitted gaussian for profile[i] */
#define NDPluginTrajectoryXWaveformString            "X_WF"                   /* (asynFloat64Array, r/o) X axis waveform */
#define NDPluginTrajectoryMuWaveformString           "MU_WF"                  /* (asynFloat64Array, r/o) Mu waveform */
#define NDPluginTrajectorySigma2WaveformString       "SIGMA2_WF"              /* (asynFloat64Array, r/o) Sigma^2 waveform */

#define NDPluginTrajectoryPeakSigmaString            "PEAK_SIGMA"             /* (asynFloat64Array, r/w) profile[i] sigma */
#define NDPluginTrajectoryPeakSigmaActualString      "PEAK_SIGMA_ACTUAL"      /* (asynFloat64Array, r/o) profile[i] sigma */
#define NDPluginTrajectoryPeakMuString               "PEAK_MU"                /* (asynFloat64Array, r/w) profile[i] mu */
#define NDPluginTrajectoryPeakMuActualString         "PEAK_MU_ACTUAL"         /* (asynFloat64Array, r/o) profile[i] mu */
#define NDPluginTrajectoryPeakAmplitudeString        "PEAK_AMPLITUDE"         /* (asynFloat64Array, r/w) profile[i] amplitude */
#define NDPluginTrajectoryPeakAmplitudeActualString  "PEAK_AMPLITUDE_ACTUAL"  /* (asynFloat64Array, r/o) profile[i] amplitude */
#define NDPluginTrajectoryBackgroundString           "BACKGROUND"             /* (asynFloat64Array, r/w) profile[i] background */
#define NDPluginTrajectoryBackgroundActualString     "BACKGROUND_ACTUAL"      /* (asynFloat64Array, r/o) profile[i] background */

#define NDPluginTrajectoryStatusString               "STATUS"                 /* (asynInt32,        r/o) status of the fitting */
#define NDPluginTrajectoryFtolString                 "FTOL"                   /* (asynFloat64,      r/w) lm_control_struct ftol value */
#define NDPluginTrajectoryXtolString                 "XTOL"                   /* (asynFloat64,      r/w) lm_control_struct xtol value */
#define NDPluginTrajectoryGtolString                 "GTOL"                   /* (asynFloat64,      r/w) lm_control_struct gtol value */
#define NDPluginTrajectoryEpsilonString              "EPSILON"                /* (asynFloat64,      r/w) lm_control_struct epsilon value */
#define NDPluginTrajectoryStepboundString            "STEPBOUND"              /* (asynFloat64,      r/w) lm_control_struct stepbound value */
#define NDPluginTrajectoryPatienceString             "PATIENCE"               /* (asynInt32,        r/w) lm_control_struct patience value */
#define NDPluginTrajectoryScaleDiagString            "SCALE_DIAG"             /* (asynInt32,        r/w) lm_control_struct scale_diag value */
#define NDPluginTrajectoryNrIterationsString         "NR_ITERATIONS"          /* (asynInt32,        r/o) lm_status_struct nfev value */
#define NDPluginTrajectoryResidVectorNormString      "RESID_VECTOR_NORM"      /* (asynFloat64,      r/o) lm_status_struct fnorm value */
#define NDPluginTrajectoryOutcomeString              "OUTCOME"                /* (asynInt32,        r/o) lm_status_struct outcome value */
#define NDPluginTrajectoryOutcomeStrString           "OUTCOME_STR"            /* (asynOctetRead,    r/o) lm_status_struct outcome value as string */
#define NDPluginTrajectoryOutcomeStrLongString       "OUTCOME_STR_LONG"       /* (asynOctetRead,    r/o) lm_status_struct outcome value as long string */

typedef enum {
	statusFitNotExecuted = 0,
	statusFitSuccessful,
	statusFitFailed,
	statusFitWrongArrayDim,
} FitStatus;

class epicsShareClass NDPluginTrajectory : public NDPluginDriver {
public:
    NDPluginTrajectory(const char *portName, int queueSize, int blockingCallbacks,
			      const char *NDArrayPort, int NDArrayAddr, int maxBuffers,
                  size_t maxMemory, int priority, int stackSize, int maxThreads);

    /* These methods override the virtual methods in the base class */
    void processCallbacks(NDArray *pArray);

    asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);

protected:
    int NDPluginTrajectoryProfileAxis;
    int NDPluginTrajectoryNumProfiles;
    int NDPluginTrajectoryPixelSize;
    int NDPluginTrajectoryMagnification;
    int NDPluginTrajectoryPosition;
    int NDPluginTrajectoryAngle;
    int NDPluginTrajectoryEmittance;
    int NDPluginTrajectoryProfileIndex;
    int NDPluginTrajectoryProfileWaveform;
    int NDPluginTrajectoryCurveWaveform;
    int NDPluginTrajectoryXWaveform;
    int NDPluginTrajectoryMuWaveform;
    int NDPluginTrajectorySigma2Waveform;
    int NDPluginTrajectoryPeakMu;
    int NDPluginTrajectoryPeakMuActual;
    int NDPluginTrajectoryPeakSigma;
    int NDPluginTrajectoryPeakSigmaActual;
    int NDPluginTrajectoryPeakAmplitude;
    int NDPluginTrajectoryPeakAmplitudeActual;
    int NDPluginTrajectoryBackground;
    int NDPluginTrajectoryBackgroundActual;
    int NDPluginTrajectoryStatus;
    int NDPluginTrajectoryFtol;
    int NDPluginTrajectoryXtol;
    int NDPluginTrajectoryGtol;
    int NDPluginTrajectoryEpsilon;
    int NDPluginTrajectoryStepbound;
    int NDPluginTrajectoryPatience;
    int NDPluginTrajectoryScaleDiag;
    int NDPluginTrajectoryNrIterations;
    int NDPluginTrajectoryResidVectorNorm;
    int NDPluginTrajectoryOutcome;
    int NDPluginTrajectoryOutcomeStr;
    int NDPluginTrajectoryOutcomeStrLong;

#define FIRST_NDPLUGIN_TRAJECTORY_PARAM NDPluginTrajectoryProfileAxis

private:
    template <typename epicsType> asynStatus getProfileT(NDArray *pArray, int axis, size_t offset, size_t size, std::vector<double> &profile);
    asynStatus getProfile(NDArray *pArray, int axis, size_t offset, size_t size, std::vector<double> &profile);
};

#endif
