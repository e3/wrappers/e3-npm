/*
 * NDPluginTrajectory.cpp
 *
 * Created March 3, 2021
 */

#include <algorithm>
#include <vector>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <iocsh.h>
#include <asynDriver.h>
#include <epicsExport.h>

#include "NDPluginTrajectory.h"
#include "lmcurve.h"

static const char *driverName = "NDPluginTrajectory";

/*
 * Gaussian
 */
static double gauss(const double x, const double *p)
{
    double B  = p[0]; // Background
    double A  = p[1]; // Amplitude
    double mu = p[2]; // Peak position
    double S2 = p[3]; // Variance (sigma^2)

    double t = x - mu;
    return B + A * exp(-0.5 * t * t / S2);
}

/*
 * Polynomial of degree 1 (line)
 */
static double poly1(const double x, const double *p)
{
    return p[0]*x + p[1];
}

/*
 * Polynomial of degree 2 (parabola)
 */
static double poly2(const double x, const double *p)
{
    return p[0]*x*x + p[1]*x + p[2];
}

/* Compute a profile vector which is the average of a number
 * of rows or columns of the image
 *
 * If axis == 0:
 *     _________________
 *    |                 |
 *    |-----------------|
 *    |  p r o f i l e  |
 *    |-----------------|
 *    |                 |
 *    |                 |
 *    |_________________|
 *
 *    profile[i] = avg(Array[offset:offset + size, i])
 *
 * if axis == 1:
 *     _________________
 *    |    | P |        |
 *    |    | r |        |
 *    |    | o |        |
 *    |    | f |        |
 *    |    | i |        |
 *    |    | l |        |
 *    |    | e |        |
 *    |____|___|________|
 *
 *    profile[i] = avg(Array[i, offset:offset + size])
 */
template <typename epicsType>
asynStatus NDPluginTrajectory::getProfileT(NDArray *pArray, int axis, size_t offset, size_t size, std::vector<double> &profile)
{
    NDArrayInfo arrayInfo;
    pArray->getInfo(&arrayInfo);

    size_t N;           // profile size
    size_t strideP;     // "profile axis" stride
    size_t strideA;     // "average axis" stride

    if (axis == 0) {
        // Average over rows
        N = arrayInfo.xSize;
        strideP = arrayInfo.xStride;
        strideA = arrayInfo.yStride;
    }
    else {
        // Average over columns
        N = arrayInfo.ySize;
        strideP = arrayInfo.yStride;
        strideA = arrayInfo.xStride;
    }

    epicsType *pData = (epicsType *)pArray->pData;

    for (size_t i = 0; i < N; i++) {
        double sum = 0;
        for (size_t j = 0; j < size; j++)
            sum += (double)pData[i*strideP + (offset + j) * strideA];

        profile[i] = sum / size;
    }

    return asynSuccess;
}

asynStatus NDPluginTrajectory::getProfile(NDArray *pArray, int axis, size_t offset, size_t size, std::vector<double> &profile)
{
    asynStatus status;

    switch(pArray->dataType) {
        case NDInt8:
            status = getProfileT<epicsInt8>(pArray, axis, offset, size, profile);
            break;
        case NDUInt8:
            status = getProfileT<epicsUInt8>(pArray, axis, offset, size, profile);
            break;
        case NDInt16:
            status = getProfileT<epicsInt16>(pArray, axis, offset, size, profile);
            break;
        case NDUInt16:
            status = getProfileT<epicsUInt16>(pArray, axis, offset, size, profile);
            break;
        case NDInt32:
            status = getProfileT<epicsInt32>(pArray, axis, offset, size, profile);
            break;
        case NDUInt32:
            status = getProfileT<epicsUInt32>(pArray, axis, offset, size, profile);
            break;
        case NDFloat32:
            status = getProfileT<epicsFloat32>(pArray, axis, offset, size, profile);
            break;
        case NDFloat64:
            status = getProfileT<epicsFloat64>(pArray, axis, offset, size, profile);
            break;
        default:
            status = asynError;
            break;
    }
    return status;
}

/** Callback function that is called by the NDArray driver with new NDArray data.
  * \param[in] pArray  The NDArray from the callback.
  */
void NDPluginTrajectory::processCallbacks(NDArray *pArray)
{
    /* It is called with the mutex already locked.
     * It unlocks it during long calculations when private structures don't
     * need to be protected.
     */
    NDArrayInfo arrayInfo;

    int axis;
    int numProfiles;
    int profileIndex;
    double position = NAN;
    double angle = NAN;
    double emittance = NAN;

    double initialFitParam[4] = { 0 };
    double fitParam[4] = { 0 };

    /* Several frames might be processed concurrently (if NumThreads > 1).
     * Use local buffers so each thread gets it's own storage.
     */
    std::vector<double> x;
    std::vector<double> mu;
    std::vector<double> sigma2;

    lm_control_struct fitControl = lm_control_double;
    lm_status_struct fitStatus;

    /* Get parameters */
    getIntegerParam(NDPluginTrajectoryProfileAxis, &axis);
    getIntegerParam(NDPluginTrajectoryNumProfiles, &numProfiles);
    getIntegerParam(NDPluginTrajectoryProfileIndex, &profileIndex);

    getDoubleParam(NDPluginTrajectoryFtol, &fitControl.ftol);
    getDoubleParam(NDPluginTrajectoryXtol, &fitControl.xtol);
    getDoubleParam(NDPluginTrajectoryGtol, &fitControl.gtol);
    getDoubleParam(NDPluginTrajectoryEpsilon, &fitControl.epsilon);
    getDoubleParam(NDPluginTrajectoryStepbound, &fitControl.stepbound);
    getIntegerParam(NDPluginTrajectoryPatience, &fitControl.patience);
    getIntegerParam(NDPluginTrajectoryScaleDiag, &fitControl.scale_diag);

    getDoubleParam(NDPluginTrajectoryBackground, &initialFitParam[0]);
    getDoubleParam(NDPluginTrajectoryPeakAmplitude, &initialFitParam[1]);
    getDoubleParam(NDPluginTrajectoryPeakMu, &initialFitParam[2]);

    double sigma;
    getDoubleParam(NDPluginTrajectoryPeakSigma, &sigma);
    initialFitParam[3] = sigma * sigma;

    if ( numProfiles > 0 && profileIndex >= numProfiles) {
        profileIndex = numProfiles - 1;
    }

    /* Call the base class method */
    NDPluginDriver::beginProcessCallbacks(pArray);

    /* This function is called with the lock taken, and it must be set when we exit.
     * The following code can be executed without the mutex because we are not accessing memory
     * that other threads can access.
     */
    this->unlock();

    pArray->getInfo(&arrayInfo);

    /* X-axis is always in beam direction. In order to allow processing in both
     * camera orientations the X and Y axes of the image might need to be swapped.
     */
    size_t xSize;
    size_t ySize;
    if (axis == 0) {
        // Horizontal profile. Beam in vertical direction
        xSize = arrayInfo.ySize;
        ySize = arrayInfo.xSize;
    }
    else {
        // Vertical profile. Beam in horizontal direction
        xSize = arrayInfo.xSize;
        ySize = arrayInfo.ySize;
    }

    std::vector<double> profile(ySize);
    std::vector<double> curve(ySize);
    std::vector<double> y(ySize);

    for (size_t i = 0; i < ySize; i++)
        y[i] = i;

    int pixelsPerSlice = xSize / numProfiles;

    /* Split the image into <numProfiles> horizontal or vertical slices.
     * For each slice, compute a profile vector by averaging over the
     * rows or columns of the slice, then fit a gaussian curve to the
     * profile.
     */
    memcpy(fitParam, initialFitParam, sizeof(fitParam));

    for (int i = 0; i < numProfiles; i++) {
        size_t offset = i * pixelsPerSlice;
        getProfile(pArray, axis, offset, pixelsPerSlice, profile);

        // Do fit
        lmcurve(4, fitParam, y.size(), y.data(), profile.data(), gauss, &fitControl, &fitStatus);

        // Check outcome
        // outcome <= 3 means fit has converged. See lm_infmsg array for meaning of outcome codes.
        bool converged = (fitStatus.outcome >= 0) && (fitStatus.outcome <= 3);

        // We are only interested in positive peaks
        if (fitParam[1] < 0) {
            converged = false;
        }

        /* Fit results are reported for one of the profiles
         * <profileIndex> determines which one
         */
        if (i == profileIndex) {
            for (unsigned j = 0; j < ySize; j++) {
                curve[j] = gauss(y[j], fitParam);
            }

            lock();
            setDoubleParam(NDPluginTrajectoryBackgroundActual, fitParam[0]);
            setDoubleParam(NDPluginTrajectoryPeakAmplitudeActual, fitParam[1]);
            setDoubleParam(NDPluginTrajectoryPeakMuActual, fitParam[2]);
            setDoubleParam(NDPluginTrajectoryPeakSigmaActual, sqrt(fitParam[3]));

            setIntegerParam(NDPluginTrajectoryOutcome, fitStatus.outcome);
            setIntegerParam(NDPluginTrajectoryNrIterations, fitStatus.nfev / 5);
            setDoubleParam(NDPluginTrajectoryResidVectorNorm, fitStatus.fnorm);

            if (converged) {
                setIntegerParam(NDPluginTrajectoryStatus, statusFitSuccessful);
            }
            else {
                setIntegerParam(NDPluginTrajectoryStatus, statusFitFailed);
            }

            if (fitStatus.outcome >= 0) {
                setStringParam(NDPluginTrajectoryOutcomeStr, lm_shortmsg[fitStatus.outcome]);
                setStringParam(NDPluginTrajectoryOutcomeStrLong, lm_infmsg[fitStatus.outcome]);
            }
            else {
                setStringParam(NDPluginTrajectoryOutcomeStr, "Unknown outcome");
                setStringParam(NDPluginTrajectoryOutcomeStrLong, "Unknown outcome");
            }

            doCallbacksFloat64Array(profile.data(), profile.size(), NDPluginTrajectoryProfileWaveform, 0);
            doCallbacksFloat64Array(curve.data(), curve.size(), NDPluginTrajectoryCurveWaveform, 0);
            unlock();
        }

        // Store results for successful fits only
        if (converged) {
            x.push_back(offset + (double)pixelsPerSlice / 2);
            mu.push_back(fitParam[2]);
            sigma2.push_back(fitParam[3]);
        }
    }

    if (x.size() > 2) {
        /* Fit line of centers */
        double ppcV[2] = {0};
        lmcurve(2, ppcV, x.size(), x.data(), mu.data(), poly1, &fitControl, &fitStatus);

        /* Fit a parabola for the beam size squared */
        double ppcV_s2[3] = {0};
        lmcurve(3, ppcV_s2, x.size(), x.data(), sigma2.data(), poly2, &fitControl, &fitStatus);

        /* Beam position at the center of the image (pixels) */
        double x0 = (double)xSize / 2;
        position = poly1(x0, ppcV);

        /* Ange of the beam (mrad) */
        angle = 1000 * ppcV[0];

        /* Emittance (pixels * mrad) */
        emittance = 1000 * sqrt(ppcV_s2[0] * ppcV_s2[2] - ppcV_s2[1] * ppcV_s2[1] / 4);
    }

    this->lock();

    doCallbacksFloat64Array(x.data(), x.size(), NDPluginTrajectoryXWaveform, 0);
    doCallbacksFloat64Array(mu.data(), mu.size(), NDPluginTrajectoryMuWaveform, 0);
    doCallbacksFloat64Array(sigma2.data(), sigma2.size(), NDPluginTrajectorySigma2Waveform, 0);

    setDoubleParam(NDPluginTrajectoryPosition, position);
    setDoubleParam(NDPluginTrajectoryAngle, angle);
    setDoubleParam(NDPluginTrajectoryEmittance, emittance);

    NDPluginDriver::endProcessCallbacks(pArray, true, true);

    callParamCallbacks();
}

/** Called when asyn clients call pasynInt32->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus  NDPluginTrajectory::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int addr=0;
    asynStatus status = asynSuccess;
    static const char *functionName = "writeInt32";

    status = getAddress(pasynUser, &addr);

    if (status != asynSuccess)
        return(status);

    /* Set the parameter in the parameter library. */
    status = (asynStatus) setIntegerParam(addr, function, value);

    /* If this parameter belongs to a base class call its method */
    if (function < FIRST_NDPLUGIN_TRAJECTORY_PARAM)
        status = NDPluginDriver::writeInt32(pasynUser, value);

     /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks(addr);

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
              "%s:%s: status=%d, function=%d, value=%d",
              driverName, functionName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%d\n",
              driverName, functionName, function, value);

    return status;
}

/** Called when asyn clients call pasynFloat64->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus  NDPluginTrajectory::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    int addr=0;
    asynStatus status = asynSuccess;
    static const char *functionName = "writeFloat64";

    status = getAddress(pasynUser, &addr);

    if (status != asynSuccess)
        return(status);

    /* Set the parameter in the parameter library. */
    status = (asynStatus) setDoubleParam(addr, function, value);

    /* If this parameter belongs to a base class call its method */
    if (function < FIRST_NDPLUGIN_TRAJECTORY_PARAM)
        status = NDPluginDriver::writeFloat64(pasynUser, value);

     /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks(addr);

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
              "%s:%s: status=%d, function=%d, value=%f",
              driverName, functionName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%f\n",
              driverName, functionName, function, value);

    return status;
}


/** Constructor for NDPluginTrajectory; most parameters are simply passed to NDPluginDriver::NDPluginDriver.
  * After calling the base class constructor this method sets reasonable default values for all of the
  * parameters.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] maxThreads The maximum number of threads this plugin is allowed to use.
  */
NDPluginTrajectory::NDPluginTrajectory(
      const char *portName, int queueSize, int blockingCallbacks,
      const char *NDArrayPort, int NDArrayAddr, int maxBuffers,
      size_t maxMemory, int priority, int stackSize, int maxThreads)
    /* Invoke the base class constructor */
    : NDPluginDriver(portName, queueSize, blockingCallbacks,
          NDArrayPort, NDArrayAddr, 1, maxBuffers, maxMemory,
          asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
          asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
		  ASYN_MULTIDEVICE, 1, priority, stackSize, maxThreads)
{
    //static const char *functionName = "NDPluginTrajectory";
    createParam(NDPluginTrajectoryProfileAxisString,          asynParamInt32,         &NDPluginTrajectoryProfileAxis);
    createParam(NDPluginTrajectoryNumProfilesString,          asynParamInt32,         &NDPluginTrajectoryNumProfiles);
    createParam(NDPluginTrajectoryPositionString,             asynParamFloat64,       &NDPluginTrajectoryPosition);
    createParam(NDPluginTrajectoryAngleString,                asynParamFloat64,       &NDPluginTrajectoryAngle);
    createParam(NDPluginTrajectoryEmittanceString,            asynParamFloat64,       &NDPluginTrajectoryEmittance);
    createParam(NDPluginTrajectoryProfileIndexString,         asynParamInt32,         &NDPluginTrajectoryProfileIndex);
    createParam(NDPluginTrajectoryProfileWaveformString,      asynParamFloat64Array,  &NDPluginTrajectoryProfileWaveform);
    createParam(NDPluginTrajectoryCurveWaveformString,        asynParamFloat64Array,  &NDPluginTrajectoryCurveWaveform);
    createParam(NDPluginTrajectoryXWaveformString,            asynParamFloat64Array,  &NDPluginTrajectoryXWaveform);
    createParam(NDPluginTrajectoryMuWaveformString,           asynParamFloat64Array,  &NDPluginTrajectoryMuWaveform);
    createParam(NDPluginTrajectorySigma2WaveformString,       asynParamFloat64Array,  &NDPluginTrajectorySigma2Waveform);

    createParam(NDPluginTrajectoryBackgroundString,           asynParamFloat64,       &NDPluginTrajectoryBackground);
    createParam(NDPluginTrajectoryBackgroundActualString,     asynParamFloat64,       &NDPluginTrajectoryBackgroundActual);
    createParam(NDPluginTrajectoryPeakAmplitudeString,        asynParamFloat64,       &NDPluginTrajectoryPeakAmplitude);
    createParam(NDPluginTrajectoryPeakAmplitudeActualString,  asynParamFloat64,       &NDPluginTrajectoryPeakAmplitudeActual);
    createParam(NDPluginTrajectoryPeakMuString,               asynParamFloat64,       &NDPluginTrajectoryPeakMu);
    createParam(NDPluginTrajectoryPeakMuActualString,         asynParamFloat64,       &NDPluginTrajectoryPeakMuActual);
    createParam(NDPluginTrajectoryPeakSigmaString,            asynParamFloat64,       &NDPluginTrajectoryPeakSigma);
    createParam(NDPluginTrajectoryPeakSigmaActualString,      asynParamFloat64,       &NDPluginTrajectoryPeakSigmaActual);

    createParam(NDPluginTrajectoryStatusString,               asynParamInt32,         &NDPluginTrajectoryStatus);
    createParam(NDPluginTrajectoryFtolString,                 asynParamFloat64,       &NDPluginTrajectoryFtol);
    createParam(NDPluginTrajectoryGtolString,                 asynParamFloat64,       &NDPluginTrajectoryGtol);
    createParam(NDPluginTrajectoryXtolString,                 asynParamFloat64,       &NDPluginTrajectoryXtol);
    createParam(NDPluginTrajectoryEpsilonString,              asynParamFloat64,       &NDPluginTrajectoryEpsilon);
    createParam(NDPluginTrajectoryStepboundString,            asynParamFloat64,       &NDPluginTrajectoryStepbound);
    createParam(NDPluginTrajectoryPatienceString,             asynParamInt32,         &NDPluginTrajectoryPatience);
    createParam(NDPluginTrajectoryScaleDiagString,            asynParamInt32,         &NDPluginTrajectoryScaleDiag);
    createParam(NDPluginTrajectoryNrIterationsString,         asynParamInt32,         &NDPluginTrajectoryNrIterations);
    createParam(NDPluginTrajectoryResidVectorNormString,      asynParamFloat64,       &NDPluginTrajectoryResidVectorNorm);
    createParam(NDPluginTrajectoryOutcomeString,              asynParamInt32,         &NDPluginTrajectoryOutcome);
    createParam(NDPluginTrajectoryOutcomeStrString,           asynParamOctet,         &NDPluginTrajectoryOutcomeStr);
    createParam(NDPluginTrajectoryOutcomeStrLongString,       asynParamOctet,         &NDPluginTrajectoryOutcomeStrLong);


    /* Set the plugin type string */
    setStringParam(NDPluginDriverPluginType, "NDPluginTrajectory");

    setIntegerParam(NDPluginTrajectoryStatus, statusFitNotExecuted);
    setStringParam(NDPluginTrajectoryOutcomeStr, "");
    setStringParam(NDPluginTrajectoryOutcomeStrLong, "");

    lm_control_struct fitControl = lm_control_double;
    setDoubleParam(NDPluginTrajectoryFtol, fitControl.ftol);
    setDoubleParam(NDPluginTrajectoryXtol, fitControl.xtol);
    setDoubleParam(NDPluginTrajectoryGtol, fitControl.gtol);
    setDoubleParam(NDPluginTrajectoryEpsilon, fitControl.epsilon);
    setDoubleParam(NDPluginTrajectoryStepbound, fitControl.stepbound);
    setIntegerParam(NDPluginTrajectoryPatience, fitControl.patience);
    setIntegerParam(NDPluginTrajectoryScaleDiag, fitControl.scale_diag);

    /* Try to connect to the array port */
    connectToArrayPort();
}

/** Configuration command */
extern "C" int NDTrajectoryConfigure(const char *portName,  int queueSize, int blockingCallbacks,
      const char *NDArrayPort, int NDArrayAddr, int maxBuffers, size_t maxMemory, int priority,
      int stackSize, int maxThreads)
{
    NDPluginTrajectory *plugin = new NDPluginTrajectory( portName, queueSize, blockingCallbacks,
          NDArrayPort, NDArrayAddr, maxBuffers, maxMemory, priority, stackSize, maxThreads);
    return plugin->start();
}

/* EPICS iocsh shell commands */
static const iocshArg initArg0 = {"portName", iocshArgString};
static const iocshArg initArg1 = {"frame queue size", iocshArgInt};
static const iocshArg initArg2 = {"blocking callbacks", iocshArgInt};
static const iocshArg initArg3 = {"NDArrayPort", iocshArgString};
static const iocshArg initArg4 = {"NDArrayAddr", iocshArgInt};
static const iocshArg initArg5 = {"maxBuffers", iocshArgInt};
static const iocshArg initArg6 = {"maxMemory", iocshArgInt};
static const iocshArg initArg7 = {"priority", iocshArgInt};
static const iocshArg initArg8 = {"stackSize", iocshArgInt};
static const iocshArg initArg9 = {"maxThreads", iocshArgInt};

static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6,
                                            &initArg7,
                                            &initArg8,
                                            &initArg9};
static const iocshFuncDef initFuncDef = {"NDTrajectoryConfigure", 10, initArgs};

static void initCallFunc(const iocshArgBuf *args)
{
    NDTrajectoryConfigure(args[0].sval, args[1].ival, args[2].ival,
                          args[3].sval, args[4].ival, args[5].ival,
                          args[6].ival, args[7].ival, args[8].ival,
                          args[9].ival);
}

extern "C" void NDTrajectoryRegister(void)
{
    iocshRegister(&initFuncDef, initCallFunc);
}

extern "C" {
epicsExportRegistrar(NDTrajectoryRegister);
}
